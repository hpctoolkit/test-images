#!/bin/bash

hpc-tsuite --help || exit $?

ls -l /usr/local/libexec/hpctoolkit-tsuite/tests || exit $?

for f in /usr/local/libexec/hpctoolkit-tsuite/tests/*; do
  echo
  echo "== Checking ldd for $(basename "$f")..."
  ldd "$f" > /tmp/ldd.log || exit $?
  grep 'not found' /tmp/ldd.log && exit 1
  rm /tmp/ldd.log
done

for f in /usr/local/libexec/hpctoolkit-tsuite/tests/*; do
  echo
  echo "== Running $(basename "$f")..."
  "$f" || exit $?
done
