from ....resources import get_binary
from ....hpctoolkit import hpcrun
from ....errors import PredictableFailure


def main():
    exe_1loop = get_binary("cpu.openmp.1loop")

    with hpcrun(exe_1loop) as M:
        if len(M.thread_stems) != 4:
            raise PredictableFailure(f"Expected exactly 4 threads, got {len(M.thread_stems)}")

        logs_found = 0
        for t in M.thread_stems:
            logs_found += 1 if M.logfile(t) else 0
            if not M.profile(t):
                raise PredictableFailure(f"Expected a profile for thread stem {t}")
            if M.tracefile(t):
                raise PredictableFailure(f"Did not expect a trace for thread stem {t}")

        if logs_found != 1:
            raise PredictableFailure(f"Expected exactly 1 log file, got {logs_found}")
