import argparse
import sys
import pkgutil
import importlib
import re
import traceback

from .errors import PredictableFailure
from . import tests as tests_module


def create_parser():
    def testspec(x):
        mat = re.fullmatch(r"(\w+(?:\.\w+)*)(?::(\w+))?", x)
        if not mat:
            raise ValueError(f"Invalid test specification {mat}")
        return mat.group(1, 2)

    parser = argparse.ArgumentParser(
        description="Run the HPCToolkit testing suite",
        epilog="Tests suites are specified by their import name (e.g. cpu.openmp), while"
        " individual tests have the name of the test after a colen (e.g. cpu.openmp:1loop)",
    )
    parser.add_argument(
        "tests", nargs="*", help="List of tests or test suites to run (default all)", type=testspec
    )
    parser.add_argument(
        "-x",
        "--xfail",
        metavar="test",
        action="append",
        help="Expect that the given test (or suite) will fail",
        type=testspec,
    )
    return parser


def discover_tests():
    def onerror(name):
        raise RuntimeError(f"Error importing module {name}")

    result = []
    for pkg in pkgutil.walk_packages(
        tests_module.__path__, tests_module.__name__ + ".", onerror=onerror
    ):
        if not pkg.ispkg and pkg.name.endswith("_test"):
            suite, test = pkg.name[len(tests_module.__name__) + 1 :].rsplit(".", 1)
            test = test[: -len("_test")]

            pkgmod = importlib.import_module(pkg.name, package=__name__)
            try:
                result.append((suite, test, pkgmod.main))
            except AttributeError:
                pass
    return result


def main():
    args = create_parser().parse_args()

    # Decipher the set of tests that will be run in this process
    to_run = discover_tests()
    if args.tests:
        to_run_suites = set(suite for suite, test in args.tests if test is None)
        to_run_tests = set((suite, test) for suite, test in args.tests if test is not None)
        to_run = list(
            filter(lambda x: x[0] in to_run_suites or (x[0], x[1]) in to_run_tests, to_run)
        )

    # Parse out the tests that we expect to fail rather than succeed
    xfail_suites, xfail_tests = [], []
    if args.xfail:
        xfail_suites = set(suite for suite, test in args.xfail if test is None)
        xfail_tests = set((suite, test) for suite, test in args.xfail if test is not None)

    # Iterate through the tests and run them!
    passed, failed, xfailed, xpassed = 0, 0, 0, 0
    for suite, test, func in to_run:
        print("=" * 70)
        print(f"Running test {suite}:{test}...")

        xfail = suite in xfail_suites or (suite, test) in xfail_tests
        try:
            func()
            if not xfail:
                print("PASSED")
            else:
                print("PASSED (unexpected and failed)")
            result = not xfail
        except PredictableFailure as e:
            traceback.print_exception(e)
            if not xfail:
                print(f"FAILED")
            else:
                print(f"FAILURE (expected and ignored)")
            result = xfail

        if result:
            if not xfail:
                passed += 1
            else:
                xfailed += 1
        else:
            if not xfail:
                failed += 1
            else:
                xpassed += 1

    print("=" * 70)
    print(f"{passed:d} passed, {xfailed:d} xfailed, {xpassed:d} xpassed, {failed:d} failed")

    sys.exit(1 if failed > 0 or xpassed > 0 else 0)


if __name__ == "__main__":
    sys.argv[0] = "python3 -m hpctoolkit_tsuite"
    main()
