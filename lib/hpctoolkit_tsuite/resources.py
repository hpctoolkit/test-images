from pathlib import Path
from configparser import ConfigParser
import os


def get_binary(name):
    if os.environ["MESON_DEVENV"]:
        # Search along HPC_TSUITE_PATH
        for root in os.environ.get("HPC_TSUITE_PATH", "").split(":"):
            trial = Path(root) / name
            if trial.is_file():
                return trial
        return None

    # Otherwise, we're in an installation so search the path indicated by the sibling file
    cfg = ConfigParser()
    with open(Path(__file__).parent / "resources.path", "r") as f:
        cfg.read_file(f)
    trial = Path(cfg["LibExecPath"]) / name
    return trial if trial.is_file() else None
