import subprocess
import contextlib
import tempfile
import shutil
from pathlib import Path
import functools
import os
import collections

from .errors import PredictableFailure


class Measurements:
    def __init__(self, basedir):
        self.basedir = Path(basedir)

    _log_suffix = ".log"
    _profile_suffix = ".hpcrun"
    _trace_suffix = ".hpctrace"
    _suffixes = (_log_suffix, _profile_suffix, _trace_suffix)

    @functools.cached_property
    def thread_stems(self):
        return set(x.stem for x in self.basedir.iterdir() if x.suffix in self.__class__._suffixes)

    def _get_file_path(self, suffixattr, stem):
        result = self.basedir / (stem + getattr(self.__class__, suffixattr))
        return result if result.is_file() else None

    logfile = functools.partialmethod(_get_file_path, "_log_suffix")
    profile = functools.partialmethod(_get_file_path, "_profile_suffix")
    tracefile = functools.partialmethod(_get_file_path, "_trace_suffix")

    def __str__(self):
        return f"{self.__class__.__name__}({self.basedir}, {len(self.thread_stems)} threads)"


# Search the PATH for the hpcrun launch script. We should be able to find it.
_hpcrun_bin = shutil.which("hpcrun")


@contextlib.contextmanager
def hpcrun(*args, timeout=30, env=None):
    exe_args = args[-1]
    if not isinstance(exe_args, list):
        exe_args = [exe_args]
    hpcrun_args = args[:-1]

    if not _hpcrun_bin:
        raise RuntimeError("hpcrun was not found on the PATH, cannot continue!")

    if env is not None:
        env = collections.ChainedMap(env, os.environ)

    with tempfile.TemporaryDirectory(prefix="hpc-tsuite-", suffix="-measurements") as mdir:
        mdir = Path(mdir)
        proc = subprocess.run(
            [_hpcrun_bin, *hpcrun_args] + ["-o", mdir, *exe_args], timeout=timeout, env=env
        )
        if proc.returncode != 0:
            raise PredictableFailure("hpcrun returned a non-zero exit code!")

        yield Measurements(mdir)
