# HPCToolkit Test Images

A repository of bite-sized applications for HPCToolkit to collect performance data from.

## Installation

### Containers

Images are available from the [container registry](https://gitlab.com/hpctoolkit/test-images/container_registry):
```
podman pull registry.gitlab.com/cpu:latest  # Tests for CPUs
podman pull registry.gitlab.com/gpu/nvidia:latest  # Tests for NVidia GPUs
```

There are no tests in common between these images.

GPU containers require additional setup to access the GPUs, see the following subsections.

#### NVidia

TODO

### Manual Installation

Install using Meson (0.63.0 or higher):
```
meson setup --prefix=<prefix> [meson options...] build/
cd build/
meson compile
meson test
meson install
```

Multiple test suites are included, some

| Test suite   | Target hardware | Additional requirements         |
|--------------|-----------------|---------------------------------|
| `cpu`        | CPUs            | None                            |
| `gpu_nvidia` | NVidia GPUs     | NVidia CUDA Toolkit and HPC SDK |

Any suite can be enabled or disabled by passing `-D<suite>=(enabled|disabled)` to the `meson setup`
command. The default for all suites is `-D<suite>=auto` which compiles as many tests as possible
based on availability of their individual requirements. (Note that `auto` may result in including
some but not all tests from a suite.)

## Usage

First install [HPCToolkit](http://hpctoolkit.org):
```
git clone https://github.com/spack/spack
source spack/share/spack/setup-env.sh
spack install hpctoolkit
spack load hpctoolkit
```

And then run some or all of the suite of tests:
```
hpc-tsuite [options...] [TESTS...]
```
See `hpc-tsuite --help` for more information.
